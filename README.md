# rustylinerc

Configure [rustyline](https://crates.io/crates/rustyline/) with user's edit mode preference.

## Emacs or Vi?

Editor wars are dumb. This tries to divine if the user prefers Emacs or vi to set the keybindings accordingly.

## Example

I haven't added it to [crates.io](https://crates.io/) yet, so add it to your dependencies in your `Cargo.toml` file like so

```toml
rustylinerc = { git = "https://gitlab.com/oylenshpeegul/rustylinerc" }
```

Then `rustylinerc::determine_edit_mode();` should return either `EditMode::Emacs` or `EditMode::Vi` for you to feed to rustyline's [edit_mode](https://docs.rs/rustyline/latest/rustyline/config/struct.Builder.html#method.edit_mode).

```rust
use rustyline::{Config, Editor};

fn main() {
    let edit_mode = rustylinerc::determine_edit_mode();
    dbg!(&edit_mode);

    let config = Config::builder().edit_mode(edit_mode).build();
    dbg!(&config);

    let mut rl: Editor<()> = Editor::with_config(config);

    let readline = rl.readline(">> ");

    match readline {
        Ok(line) => println!("Line: {:?}", line),
        Err(_) => println!("No input"),
    }
}
```
