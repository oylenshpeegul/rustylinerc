use rustylinerc;

#[test]
fn test_inputrc() {
    temp_env::with_vars(
        vec![
            ("EDITRC", None),
            ("HOME", None),
            ("INPUTRC", Some("./tests/inputrc_emacs")),
        ],
        || {
            let edit_mode = rustylinerc::determine_edit_mode();
            assert_eq!(edit_mode, rustyline::EditMode::Emacs);
        }
    );

    temp_env::with_vars(
        vec![
            ("EDITRC", None),
            ("HOME", None),
            ("INPUTRC", Some("./tests/inputrc_vi")),
        ],
        || {
            let edit_mode = rustylinerc::determine_edit_mode();
            assert_eq!(edit_mode, rustyline::EditMode::Vi);
        }
    );

    temp_env::with_vars(
        vec![
            ("EDITRC", None),
            ("HOME", None),
            ("INPUTRC", Some("./tests/inputrc_vi2")),
        ],
        || {
            let edit_mode = rustylinerc::determine_edit_mode();
            assert_eq!(edit_mode, rustyline::EditMode::Vi);
        }
    );
}

#[test]
fn test_editrc() {
    temp_env::with_vars(
        vec![
            ("EDITRC", Some("./tests/editrc_emacs")),
            ("HOME", None),
            ("INPUTRC", None),
        ],
        || {
            let edit_mode = rustylinerc::determine_edit_mode();
            assert_eq!(edit_mode, rustyline::EditMode::Emacs);
        }
    );

    temp_env::with_vars(
        vec![
            ("EDITRC", Some("./tests/editrc_vi")),
            ("HOME", None),
            ("INPUTRC", None),
        ],
        || {
            let edit_mode = rustylinerc::determine_edit_mode();
            assert_eq!(edit_mode, rustyline::EditMode::Vi);
        }
    );
}
