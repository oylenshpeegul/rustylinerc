use std::{env, fs};
use rustyline::EditMode;
use std::path::PathBuf;

// GNU Readline checks INPUTRC, ~/.inputrc, /etc/inputrc
//
// Korn shell checks VISUAL, EDITOR
//
// BSD Editline checks EDITRC, ./.editrc, ~/.editrc
//
pub fn determine_edit_mode() -> EditMode {
    if let Some(inputrc) = find_inputrc() {
        let contents = fs::read_to_string(inputrc).expect("Cannot read inputrc file!");
        let vi = contents
            .lines()
            .filter(|line| !line.starts_with('#'))
            .map(|line| line.trim())
            .filter(|line| *line == "set editing-mode vi" || *line == "set -o vi")
            .collect::<Vec<_>>();
        if !vi.is_empty() {
            return EditMode::Vi;
        }
    };

    if let Ok(visual) = env::var("VISUAL") {
        if visual.starts_with("vi") {
            return EditMode::Vi;
        }
    }

    if let Ok(editor) = env::var("EDITOR") {
        if editor.starts_with("vi") {
            return EditMode::Vi;
        }
    }

    if let Some(editrc) = find_editrc() {
        let contents = fs::read_to_string(editrc).expect("Cannot read editrc file!");
        let vi = contents
            .lines()
            .filter(|line| !line.starts_with('#'))
            .map(|line| line.trim())
            .filter(|line| *line == "bind -v")
            .collect::<Vec<_>>();
        if !vi.is_empty() {
            return EditMode::Vi;
        }
    };
    
    EditMode::Emacs
}

// Look for an inputrc file.
//
// From the `readline` docs:
//
// The name of this file is taken from the value of the environment
// variable INPUTRC. If that variable is unset, the default is
// `~/.inputrc'. If that file does not exist or cannot be read, the
// ultimate default is `/etc/inputrc'.
//
pub fn find_inputrc() -> Option<PathBuf> {
    if let Ok(inputrc) = env::var("INPUTRC") {
        return Some(PathBuf::from(inputrc));
    }

    if let Ok(home) = env::var("HOME") {
        let mut inputrc = PathBuf::from(home);
        inputrc.push(".inputrc");
        if inputrc.is_file() {
            return Some(inputrc);
        }
    }

    let inputrc = PathBuf::from("/etc/inputrc");
    if inputrc.exists() {
        return Some(inputrc);
    }

    None
}

// Look for an editrc file.
//
// From the `editline` docs:
//
// The name of this file is taken from the value of the environment
// variable EDITRC. If that variable is unset, try `$PWD/.editrc` then
// `$HOME/.editrc'.
//
pub fn find_editrc() -> Option<PathBuf> {
    if let Ok(editrc) = env::var("EDITRC") {
        return Some(PathBuf::from(editrc));
    }

    let editrc = PathBuf::from("./.editrc");
    if editrc.is_file() {
        return Some(editrc);
    }

    if let Ok(home) = env::var("HOME") {
        let mut editrc = PathBuf::from(home);
        editrc.push(".editrc");
        if editrc.is_file() {
            return Some(editrc);
        }
    }

    None
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_editrc() {
        temp_env::with_vars(
            vec![
                ("EDITRC", None),
                ("HOME", None), // Don't find ~/.editrc
                ("INPUTRC", Some("bar")),
            ],
            || {
                let editrc = find_editrc();
                assert_eq!(editrc, None);
            },
        );

        temp_env::with_vars(
            vec![
                ("EDITRC", Some("foo")),
                ("INPUTRC", Some("bar")),
            ],
            || {
                let editrc = find_editrc();
                assert_eq!(editrc, Some(PathBuf::from("foo")));
            },
        );
    }

    #[test]
    fn test_find_inputrc() {
        temp_env::with_vars(
            vec![
                ("EDITRC", Some("foo")),
                ("HOME", None), // Don't find ~/.inputrc
                ("INPUTRC", None),
            ],
            || {
                let inputrc = find_inputrc();
                let etc_inputrc = PathBuf::from("/etc/inputrc");
                if etc_inputrc.exists() {
                    assert_eq!(inputrc, Some(etc_inputrc));
                } else {
                    assert_eq!(inputrc, None);
                }
            },
        );

        temp_env::with_vars(
            vec![
                ("EDITRC", Some("foo")),
                ("INPUTRC", Some("bar")),
            ],
            || {
                let inputrc = find_inputrc();
                assert_eq!(inputrc, Some(PathBuf::from("bar")));
            },
        );
    }
}
